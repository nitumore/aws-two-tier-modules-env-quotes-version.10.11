
/home/siac/terraform.11.14 init

sed -i.bak "s/\(^[ ]*source[ ]*=[ ]*\"\)/\1.\//g" env-stage101.tf
sed -i.bak "s/\(^[ ]*source[ ]*=[ ]*\"\)/\1.\//g" env-stage212.tf

rm env-stage101.tf.bak
rm env-stage212.tf.bak

/home/siac/terraform.12.20 init -upgrade
/home/siac/terraform.12.20 validate
/home/siac/terraform.12.20 0.12upgrade -yes

/home/siac/terraform.12.20 plan -out tfplan.out > tfplan.raw

/home/siac/terraform.12.20 show -json tfplan.out > tfplan.json